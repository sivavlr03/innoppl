<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function index()
	{
		$data['search'] = $this->input->post('search');
		$data['filter'] = $this->input->post('search_filter');
		
		$this->load->model('Employee_model');
		$data['list'] = $this->Employee_model->employee_table($data);
		$this->load->view('welcome_message', $data);
	}
}
