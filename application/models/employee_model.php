<?php  
 class Employee_model extends CI_Model  
 {  
     function employee_table($data = array())  
     {  

          if($data['filter'] == '')
          {
               $result = $this->getEmployeeList($data);
          }
          else if($data['filter'] == 1)
          {
               $result = $this->getAverageSalaryofDepartment($data);
          }
          else if($data['filter'] == 2)
          {
               $result = $this->getHighestAverageSalaryofDepartment($data);
          }
          else if($data['filter'] == 3)
          {
               $result = $this->getTop10PaidEmployee($data);
          }
          else if($data['filter'] == 4)
          {
               $result = $this->getEmployeeByDobMonth($data);
          }
          else if($data['filter'] == 5)
          {
               $result = $this->getEmployeeByExperience($data);
          }
          return $result;
     } 
     
     function getEmployeeList($data = array())  
     {

          $search = $data['search'];
          $empList = "SELECT 
               master.id as master_id,
               emp.*,
               dep.d_name as department,
               role.r_name as role,
               salary.s_amount as salary 
          FROM 
               `master_table` master LEFT JOIN employee emp on master.employee = emp.e_id 
               left join department dep on master.department=dep.d_id 
               left join role on role.r_id = master.role 
               left join salary on master.salary = salary.s_id 
          where master.status= 0 ";

          if(trim($search) != '')
          {
               $empList .= " and (dep.d_name like '%".$search."%' or emp.e_name like '%".$search."%' or role.r_name like '%".$search."%')";

          }

          $getList = $this->db->query($empList);  
          $result = $getList->result_array();
          return $result;
     }

     function getAverageSalaryofDepartment($data = array())  
     {

          $search = $data['search'];
          $empList = "SELECT 
               AVG(b.s_amount) as avg_amount,
               c.d_name as department,
               e.e_name as head
          FROM 
               master_table a left JOIN salary b on a.salary = b.s_id 
               left join department c on a.department =c.d_id 
               left join employee e on e.e_id = a.employee";

          if(trim($search) != '')
          {
               $empList .= " where c.d_name like '%".$search."%' ";
          }else{
               $empList .= " GROUP by c.d_name order by avg_amount";
          }

          $getList = $this->db->query($empList);  
          $result = $getList->result_array();
          return $result;
     }
     function getHighestAverageSalaryofDepartment($data = array())  
     {

          $search = $data['search'];
          $empList = "SELECT 
               AVG(b.s_amount) as avg_amount,
               c.d_name as department,
               e.e_name as head
          FROM 
               master_table a left JOIN salary b on a.salary = b.s_id 
               left join department c on a.department =c.d_id
               left join employee e on e.e_id = a.employee 
               ";

          if(trim($search) != '')
          {
               $empList .= " where c.d_name like '%".$search."%' ";
          }else{
               $empList .= " GROUP by c.d_name ORDER by AVG(b.s_amount) desc limit 1";
          }

          $getList = $this->db->query($empList);  
          $result = $getList->result_array();
          return $result;
     }

     function getTop10PaidEmployee($data = array())  
     {
          $top5List = "select DISTINCT s.s_amount as amount from master_table m left join salary s on m.salary= s.s_id order by s.s_amount desc limit 0,5";

          $getListAmount = $this->db->query($top5List);  
          $amountRes = $getListAmount->result_array();
          $amountList = array();
          foreach($amountRes as $key => $val)
          {
               $amountList[] = $val['amount'];
 
          }

          $amountContrainStr = implode("','",$amountList);

          $search = $data['search'];
          $empList = "SELECT 
               master.id as master_id,
               emp.*,
               dep.d_name as department,
               role.r_name as role,
               salary.s_amount as salary 
          FROM 
               `master_table` master LEFT JOIN employee emp on master.employee = emp.e_id 
               left join department dep on master.department=dep.d_id 
               left join role on role.r_id = master.role 
               left join salary on master.salary = salary.s_id 
          where master.status= 0 ";

          if(trim($search) != '')
          {
               $empList .= " and (dep.d_name like '%".$search."%' or emp.e_name like '%".$search."%' or role.r_name like '%".$search."%')";

          }

          $empList .= " and salary.s_amount in ('".$amountContrainStr."') ";

          $getList = $this->db->query($empList);  
          $result = $getList->result_array();
          return $result;
     }

     function getEmployeeByDobMonth($data = array())  
     {

          $search = $data['search'];
          $empList = "SELECT 
               master.id as master_id,
               emp.*,
               dep.d_name as department,
               role.r_name as role,
               salary.s_amount as salary 
          FROM 
               `master_table` master LEFT JOIN employee emp on master.employee = emp.e_id 
               left join department dep on master.department=dep.d_id 
               left join role on role.r_id = master.role 
               left join salary on master.salary = salary.s_id 
          where master.status= 0 
          
         ";

          $months = array(
               'january',
               'february',
               'march',
               'april',
               'may',
               'june',
               'july ',
               'august',
               'september',
               'october',
               'november',
               'december',
          );

          if(trim($search) != '' && in_array(strtolower($search),$months))
          {
               $index = array_search(strtolower($search), array_values($months));
               $empList .= " and month(emp.e_dob) = ".($index+1);
          }else{
               $empList .= " and month(emp.e_dob) = ".date('m');
          }


          $getList = $this->db->query($empList);  
          $result = $getList->result_array();
          return $result;
     }

     function getEmployeeByExperience($data = array())  
     {

          $search = $data['search'];
          $empList = "SELECT 
               master.id as master_id,
               emp.*,
               dep.d_name as department,
               role.r_name as role,
               salary.s_amount as salary 
          FROM 
               `master_table` master LEFT JOIN employee emp on master.employee = emp.e_id 
               left join department dep on master.department=dep.d_id 
               left join role on role.r_id = master.role 
               left join salary on master.salary = salary.s_id 
          where master.status= 0 and emp.e_doj > '".date('Y-m-d', strtotime('-10 years'))."'";

          if(trim($search) != '')
          {
               $empList .= " and (dep.d_name like '%".$search."%' or emp.e_name like '%".$search."%' or role.r_name like '%".$search."%')";

          }

          $empList .= " order by emp.e_doj ";

          $getList = $this->db->query($empList);  
          $result = $getList->result_array();
          return $result;
     }


 } 
 
 ?>