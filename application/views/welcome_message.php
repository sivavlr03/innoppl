<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Innoppl Technologies - Task Assestment</title>
	<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/datatable/datatables.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

	<style>
		.table
		{
			font-size: 0.9rem;
		}
	</style>
</head>

<body>


	<div class="container">
		
		<h4 style="padding-top:25px;">Employee Management</h4>
		<hr>
		<form action="" method="post">
		<div class="row">
			<div class="col-4">
                <label for="search">Search</label>
                <input type="text" class="form-control" id="search" name="search" placeholder="Keyword" value="<?php echo $search; ?>">
				<?php 
				$alert = 'Keyword can be Employee Name, Department Name and Role';
				if($filter == 1 or $filter == 2)
				{
					$alert = 'Keyword can be Department Name';
				}else if ($filter == 4 )
				{
					$alert = 'Keyword can be Month Name like "march" ';
				}
			
				?>
				<small id='filter_key'><?=$alert?></small>
            </div>
			<div class="col-6">
                <label for="search_filter">Filter</label>
				<select class="custom-select" id="search_filter" name="search_filter" >
                  <option value="">--Select--</option>
                  <option value='1' <?php echo $filter == '1' ? ' selected="selected"': ''; ?>>Average salary of the department</option>
                  <option value='2' <?php echo $filter == '2' ? ' selected="selected"': ''; ?>>Highest average salary of the department </option>
                  <option value='3' <?php echo $filter == '3' ? ' selected="selected"': ''; ?>>Top 5 highest-paid employees</option>
                  <option value='4' <?php echo $filter == '4' ? ' selected="selected"': ''; ?>>Employee who born this month</option>
                  <option value='5' <?php echo $filter == '5' ? ' selected="selected"': ''; ?>>Employee who has more than 10 years of experience and sort by date</option>
                </select>
            </div>
			<div class="col-2">
				<button class="btn btn-primary btn-block" style="margin-top: 30px;" type="submit" name="submit" id="submit">Submit</button>
            </div>
        </div>
		</form>
		<div class="row mt-3">
			<div class="col-12">

				<table class="table table-bordered table-stripped table-hover" id="table">
				<?php if($filter == '' or $filter == '3' or $filter == '4' or $filter == '5') { ?>
					<thead>
					<tr>
						<th>Employee ID</th>
						<th>Employee Name</th>
						<th>DOB</th>
						<th>Address</th>
						<th>Contact</th>
						<th>Date of Joining</th>
						<th>Department</th>
						<th>Role</th>
						<th>Salary</th>
					</tr>
					</thead>
					<tbody>
					<?php 

						foreach($list as $key => $val)
						{
							echo "<tr><td>".$val['e_id']."</td><td>".$val['e_name']."</td><td>".$val['e_dob']."</td><td>".$val['e_address']."</td><td>".$val['e_contact']."</td><td>".$val['e_doj']."</td><td>".$val['department']."</td><td>".$val['role']."</td><td>".$val['salary']."</td></tr>";
						}

					?>
					</tbody>
				<?php } else if($filter == 1 or $filter == 2) { ?>

					<thead>
						<th>Department Name</th>
						<th>Average Salary</th>
						<th>Manager</th>
					</thead>
					<tbody>
					<?php 

						foreach($list as $key => $val)
						{
							echo "<tr><td>".$val['department']."</td><td>".$val['avg_amount']."</td><td>".$val['head']."</td></tr>";
						}
					?>

					</tbody>

				<?php } ?>
			</table>

		</div>
		</div>

	</div>


	<footer class="pt-2 text-muted text-center text-small">
        <p class="mb-1"><?php echo date('Y'); ?>© Company Name</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="#">Terms</a></li>
          <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
      </footer>


	<script src="<?php echo base_url(); ?>assets/jquery/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/datatable/datatables.js"></script>
	<script src="<?php echo base_url(); ?>assets/datatable/Buttons-1.7.1/js/dataTables.buttons.js"></script>
	<script src="<?php echo base_url(); ?>assets/datatable/Buttons-1.7.1/js/buttons.html5.min.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').DataTable({searching: false, info: false,
				dom: 'Bfrtip',
				buttons: [
					'excel','csv'
				]
				<?php if($filter == 3 ){ ?>,"order": [[ 8, "desc" ]]<?php } ?>
				<?php if($filter == 5 ){ ?>,"order": [[ 5, "desc" ]]<?php } ?>
			});
		});

		
		$(document).on('change','#search_filter',function(){
			var key ='Keyword can be Employee Name, Department Name and Role';
			if($(this).val() == '1' || $(this).val() == '2')
			{
				$('#filter_key').html('Keyword can be Department Name');
			}else if($(this).val() == '4')
			{
				$('#filter_key').html('Keyword can be Month Name like "march" ');
			}else{
				$('#filter_key').html(key);
			}
		});
	</script>
</body>

</html>