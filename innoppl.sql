-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2021 at 10:41 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `innoppl`
--
CREATE DATABASE IF NOT EXISTS `innoppl` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `innoppl`;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `d_id` smallint(6) NOT NULL,
  `d_name` varchar(100) NOT NULL,
  `d_head` mediumint(9) NOT NULL,
  `d_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`d_id`, `d_name`, `d_head`, `d_status`) VALUES
(1, 'IT', 5, 0),
(2, 'Finace', 3, 0),
(3, 'HR', 9, 0),
(4, 'Production', 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `e_id` mediumint(9) NOT NULL,
  `e_name` varchar(100) NOT NULL,
  `e_dob` date NOT NULL,
  `e_address` varchar(200) NOT NULL,
  `e_contact` bigint(20) NOT NULL,
  `e_doj` date NOT NULL,
  `e_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`e_id`, `e_name`, `e_dob`, `e_address`, `e_contact`, `e_doj`, `e_status`) VALUES
(1, 'siva', '1991-02-27', 'Vellore', 9944915579, '2015-08-05', 0),
(2, 'Ram', '1980-06-05', 'Chennai', 9944915579, '2010-08-06', 0),
(3, 'Kumar', '1985-03-01', 'Vellore', 9944915579, '2005-08-07', 0),
(4, 'Vinoth', '1991-03-02', 'Chennai', 9944915579, '2015-08-08', 0),
(5, 'Prabha', '1985-08-03', 'Chennai', 9944915579, '2003-08-09', 0),
(6, 'Prabhu', '1994-03-04', 'Vellore', 9944915579, '2011-08-10', 0),
(7, 'Arun', '1991-03-05', 'Chennai', 9944915579, '2012-08-11', 0),
(8, 'Venket', '1991-03-06', 'Chennai', 9944915579, '2015-08-12', 0),
(9, 'Anand', '1991-03-07', 'Chennai', 9944915579, '2015-08-13', 0),
(10, 'Mohan', '1991-03-08', 'Vellore', 9944915579, '2015-08-14', 0),
(11, 'Abdul', '1991-03-09', 'Vellore', 9944915579, '2015-08-15', 0),
(12, 'Peter', '1991-03-10', 'Chennai', 9944915579, '2015-08-16', 0),
(13, 'Naresh', '1991-03-11', 'Chennai', 9944915579, '2015-08-17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_table`
--

CREATE TABLE `master_table` (
  `id` int(11) NOT NULL,
  `employee` mediumint(9) NOT NULL,
  `department` mediumint(9) NOT NULL,
  `role` mediumint(9) NOT NULL,
  `salary` mediumint(9) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_table`
--

INSERT INTO `master_table` (`id`, `employee`, `department`, `role`, `salary`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 1, 3, 6, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(2, 2, 1, 4, 7, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(3, 3, 2, 11, 10, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(4, 4, 1, 3, 6, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(5, 5, 1, 6, 14, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(6, 6, 2, 8, 7, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(7, 7, 1, 2, 4, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(8, 8, 2, 9, 8, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(9, 9, 3, 17, 10, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(10, 10, 4, 22, 11, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(11, 11, 4, 19, 6, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(12, 12, 4, 20, 7, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0),
(13, 13, 3, 13, 8, '2021-08-08 20:40:13', '2021-08-08 20:40:13', 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `r_id` smallint(6) NOT NULL,
  `r_name` varchar(100) NOT NULL,
  `d_id` mediumint(9) NOT NULL,
  `r_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`r_id`, `r_name`, `d_id`, `r_status`) VALUES
(1, 'Software Trainee', 1, 0),
(2, 'Software Developer', 1, 0),
(3, 'Sr. Software Developer', 1, 0),
(4, 'Team Leader', 1, 0),
(5, 'Asst.Manager', 1, 0),
(6, 'Manager', 1, 0),
(7, 'Accountant Trainee', 2, 0),
(8, 'Accountant ', 2, 0),
(9, 'Sr. Accountant ', 2, 0),
(10, 'Asst.Manager', 2, 0),
(11, 'Manager', 2, 0),
(12, 'HR Trainee', 3, 0),
(13, 'HR Executive', 3, 0),
(14, 'Sr. HR', 3, 0),
(15, 'HR Operation', 3, 0),
(16, 'Asst.Manager', 3, 0),
(17, 'Manager', 3, 0),
(18, 'Research Analysist Trainee', 4, 0),
(19, 'Research Analysist', 4, 0),
(20, 'Sr. Research Analysist', 4, 0),
(21, 'Team Leader', 4, 0),
(22, 'Manager', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `s_id` smallint(6) NOT NULL,
  `s_amount` int(11) NOT NULL,
  `s_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`s_id`, `s_amount`, `s_status`) VALUES
(1, 12000, 0),
(2, 15000, 0),
(3, 18000, 0),
(4, 20000, 0),
(5, 25000, 0),
(6, 30000, 0),
(7, 35000, 0),
(8, 40000, 0),
(9, 45000, 0),
(10, 50000, 0),
(11, 60000, 0),
(12, 75000, 0),
(13, 80000, 0),
(14, 100000, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`d_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `master_table`
--
ALTER TABLE `master_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`s_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `d_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `e_id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `master_table`
--
ALTER TABLE `master_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `r_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `s_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
